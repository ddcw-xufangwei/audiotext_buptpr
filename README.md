Praat文本标注工具使用文档

一．	Praat的基本使用
1.	打开Praat---左上角Open---Read from file...---找到对应的声音或者Textrid文件打开即可
 
2.	打开到Praat窗口以后，创建一个空白的标注文件（TextGrid文件）
 
Annotate---To TextGrid...
 
第一栏是标注文件名，第二栏是标记标注的关键层，我们这里不需要设置
标注文件名保持与音频文件命名相同即可
3.	把声音文件和TextGrid文件按住ctrl同时选中，点击View & Edit，就可以进行标注 
4.	标注时，根据听辨边界，以及查看语图的信息，确定音素或者音节的边界。
 
5.	标注过程中，在最上方音频文件部分点击选择边界位置，出现红色虚线，在下方标注文件部分会出现上方有小圆钮的灰色粗线，在确定好一句话的边界位置后，点击标注文件部分灰色粗线上的小圆钮，确认划分边界。
 
6.	划分好边界后在上方文本栏键入人物所说的话，具体可参考标注实例文件，语气词和重复口头语可不用重复标注
 
7.	标注完成后ctrl + s 即可自动分类保存







二．	可能出现的常见问题
1.	如何取消边界？
选择一条边界后点击上方菜单栏中的boundary，然后点击cancel
2.	分类
三．	脚本使用方法
在掌握了Praat的基本操作之后，要知道如何运行一个脚本。  
1. 先打开Praat软件点击Praat, Open praat script...
2. 找到需要打开的脚本，
  `
3.	弹出脚本界面
 
4.	点击run，运行脚本










四．	参考文献
# speech

audio related

## audio_process_merge.py:

使用rnnoise进行降噪的示例程序

依赖工程：

	https://github.com/xiph/rnnoise

	https://github.com/Shb742/rnnoise_python

其中的DeapSpeakerModel为deep-speaker词向量嵌入工程（readme第三部分）。

使用到的库：moviepy，使用pip安装，conda安装可能会出现找不到库的问题。

## audio_editor.py:

webrtc进行vad检测，读视频，提音频，换sr从44100到32000，每30ms检测一次。

## deep-speaker：

词向量嵌入工程（https://github.com/philipperemy/deep-speaker），可以进行根据语音判断是否是同一个人。

## deep speech

中文识别，百度的deep speech效果会相对好，paddle框架，只支持python2.7，需要使用docker安装。

	https://github.com/PaddlePaddle/DeepSpeech

在3号机/home/DataBase4/cdh/speech/baidu_cn1.2k_model目录，有下载完全的相应模型和配置文件。

## asrt

星标数比较多的开源中文语音识别工程，3号机/home/DataBase3/cdh/speech目录有语音相关的常用数据集。

## textgrid_process.py and test_denoised.TextGrid

使用tgt库对praat生成的textgrid文件进行读取

tgt:

	https://github.com/hbuschme/TextGridTools/tree/5e58d13923f5e513c56667f157e6a5091e40f00d

	https://textgridtools.readthedocs.io/en/stable/api.html

praat:语音数据标注工具

	https://praat.en.softonic.com/download#

	https://zhuanlan.zhihu.com/p/113195536

	https://blog.csdn.net/shaopengfei/article/details/108631858

	https://blog.csdn.net/shaopengfei/article/details/109378097

	https://blog.csdn.net/shaopengfei/article/details/109097404
